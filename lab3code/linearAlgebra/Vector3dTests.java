package linearAlgebra;


import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class Vector3dTests {
    @Test
    public void testGetMethods(){
        Vector3d v = new Vector3d(10, 11, 12);
        assertEquals(10, v.getX());
        assertEquals(11, v.getY());
        assertEquals(12, v.getZ());
    }

    @Test
    public void testMagnitude(){
        Vector3d v = new Vector3d(15, 17, 19);
        assertEquals(5 * Math.sqrt(35), v.magnitude());
    }

    @Test
    public void testDotProduct(){
        Vector3d v = new Vector3d(1,2,3);
        Vector3d e = new Vector3d(4, 5, 6);
        assertEquals(32, v.dotProduct(e));
    }

    @Test
    public void testAdd(){
        Vector3d v = new Vector3d(10, 12, 14);
        Vector3d e = new Vector3d(20, 19, 18);
        Vector3d a = v.add(e);
        assertEquals(30, a.getX());
        assertEquals(31, a.getY());
        assertEquals(32, a.getZ());
    }
}