// Mauricio Murillo
// ID: 2041723

package linearAlgebra;
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt((Math.pow(x, 2))+(Math.pow(y, 2))+(Math.pow(z, 2)));
    }

    public double dotProduct(Vector3d v){
        double d1 = v.getX() * this.x;
        double d2 = v.getY() * this.y;
        double d3 = v.getZ() * this.z;

        return d1 + d2 + d3;
    }

    public Vector3d add(Vector3d v){
        double a1 = v.getX() + this.x;
        double a2 = v.getY() + this.y;
        double a3 = v.getZ() + this.z;

        return new Vector3d(a1, a2, a3);
    }
}